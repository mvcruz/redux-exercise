import { Reducer } from 'redux';
import { AppAction } from 'state';
import {numberOperation} from "../actions";


type contentHigherOrderState = {
  entryNumber: {
    expression: string;
  },
  entryOperation: {
    stack: number[];
  }
}

type HigherOrderState = {
  content: contentHigherOrderState;
  stack: number[];
  history: any[];
  expression: string,
};

function getInitialState<contentHigherOrderState>(reducer: Reducer<contentHigherOrderState, any>): contentHigherOrderState {
  return reducer(undefined, { type: undefined });
}

export function higherOrderReducer(
  reducer: Reducer<contentHigherOrderState, AppAction>
): Reducer<HigherOrderState, AppAction> {
  const initialState = {
    content: getInitialState(reducer),
    stack: [],
    history: [],
    expression: "",
  };
  return (state = initialState, action: AppAction) => {
    let contentUpdated = reducer(state.content, action);
    switch (action.type){
      case 'ENTER_INTRO':
        if(state.expression === ""){
          break;
        }
        state.stack.push(Number(contentUpdated.entryNumber.expression));
        state.expression = "";
        reducer(state.content, numberOperation(""));
        break;
      case 'ENTER_NUMBER':
        state.expression = contentUpdated.entryNumber.expression;
        break;
      case 'ENTER_UNDO':
        if (state.history.length === 0) {
          break;
        }
        state.history.pop();
        if (state.history.length === 0) {
          state.expression = "";
          state.stack = [];
          break;
        }
        let oldState = state.history.pop();
        state.expression = oldState[0];
        state.stack = oldState[1];
        reducer(state.content, numberOperation(""));
        reducer(state.content, numberOperation(state.expression));
        break;
      case 'SUM':
      case 'SUB':
      case 'MUL':
      case 'DIV':
      case 'SUM_ALL':
      case 'SQRT':
        state.stack = contentUpdated.entryOperation.stack;
        break;
    }
    state.history.push([state.expression, Object.assign([], state.stack)]);
    return {
      content: contentUpdated,
      stack: state.stack,
      history: state.history,
      expression: state.expression,
    };
  };
}
