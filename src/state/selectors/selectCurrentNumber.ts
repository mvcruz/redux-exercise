import { RootState } from '..';

export const selectCurrentNumber = (state: RootState): number => {
  if (state.expression === undefined) {
    return 0;
  }

  return Number(state.expression);
};
