import { useDispatch, useSelector } from 'react-redux';

import {numberOperation} from 'state/actions/numberOperation';
import {enterIntro, enterUndo} from 'state/actions/keyboardOperation';
import {enterSqrt, enterSum, enterSumAll} from "../state/actions";

import { selectCurrentNumber } from 'state/selectors/selectCurrentNumber';
import { selectCurrentStack } from 'state/selectors/selectCurrentStack';

import styles from './Calculator.module.css';
import {enterDiv, enterMul, enterSub} from "../state/actions";

const renderStackItem = (value: number, index: number) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();

  const onClickNumber = (n: number) => {
    const action = numberOperation(n.toString());
    dispatch(action);
  };

  const onClickDot = () => {
    const action = numberOperation(".");
    dispatch(action);
  };

  const onClickIntro = () => {
    const action = enterIntro();
    dispatch(action);
  }

  const onClickUndo = () => {
    const action = enterUndo();
    dispatch(action);
  }

  const onClickSub = () => {
    const action = enterSub(stack);
    dispatch(action);
  }

  const onClickMul = () => {
    const action = enterMul(stack);
    dispatch(action);
  }

  const onClickDiv = () => {
    const action = enterDiv(stack);
    dispatch(action);
  }

  const onClickSum = () => {
    const action = enterSum(stack);
    dispatch(action);
  }

  const onClickSqrt = () => {
    const action = enterSqrt(stack);
    dispatch(action);
  }

  const onClickSumAll = () => {
    const action = enterSumAll(stack);
    dispatch(action);
  }

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onClickDot()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onClickSum()}>+</button>
        <button onClick={() => onClickSub()}>-</button>
        <button onClick={() => onClickMul()}>x</button>
        <button onClick={() => onClickDiv()}>/</button>
        <button onClick={() => onClickSqrt()}>√</button>
        <button onClick={() => onClickSumAll()}>Σ</button>
        <button onClick={() => onClickUndo()}>Undo</button>
        <button onClick={() => onClickIntro()}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
