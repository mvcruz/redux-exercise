import { Reducer } from 'redux';
import { AppAction } from '../AppAction';

type mathState = {
    stack: number[];
};

const initialState: mathState = {
    stack: [],
};

function performMathBinaryOperation(state: mathState, operation: string){
    if(state.stack.length === 1 || state.stack.length === 0){
        return;
    }
    let number1 = Number(state.stack.pop());
    let number2 = Number(state.stack.pop());

    switch (operation){
        case 'SUM':
            state.stack.push(number2 + number1);
            break;
        case 'SUB':
            state.stack.push(number2 - number1);
            break;
        case 'MUL':
            state.stack.push(number2 * number1);
            break;
        case 'DIV':
            state.stack.push(number2 / number1);
    }
    return;
}

export const mathReducer: Reducer<mathState, AppAction> = (
    state = initialState,
    action
) => {
    switch (action.type) {
        case 'SQRT':
            state.stack = action.stack;
            if(state.stack.length === 0){
                return state;
            }
            state.stack.push(Math.sqrt( Number( state.stack.pop() ) ) );
            return state;
        case 'SUM_ALL':
            state.stack = action.stack;
            if(state.stack.length === 0){
                return state;
            }
            let result = Number(state.stack.pop());
            state.stack.forEach( (x) => {
                result += Number(x);
            } );
            state.stack = [];
            state.stack.push(result);
            return state;

        case 'SUM':
        case 'SUB':
        case 'MUL':
        case 'DIV':
            state.stack = action.stack;
            performMathBinaryOperation(state, action.type);
            return state;
        default:
            return state;
    }
};