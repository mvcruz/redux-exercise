import { combineReducers } from 'redux';

import { numberReducer } from './numberReducer';
import { higherOrderReducer } from './higherOrderReducer';
import {mathReducer} from "./mathReducer";

export const rootReducer = higherOrderReducer(
  combineReducers({
    entryNumber: numberReducer,
    entryOperation: mathReducer,
  })
);
