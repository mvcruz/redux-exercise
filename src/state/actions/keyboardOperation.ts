type EnterKeyboardAction = {
    type: 'ENTER_INTRO' | 'ENTER_UNDO';
};

export const enterIntro = (): EnterKeyboardAction => ({
    type: 'ENTER_INTRO',
});

export const enterUndo = (): EnterKeyboardAction => ({
    type: 'ENTER_UNDO',
});