import { RootState } from '..';

export const selectCurrentStack = (state: RootState): number[] => {
  if (state.stack === undefined) {
    return [];
  }

  return state.stack;

};
