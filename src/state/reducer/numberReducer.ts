import { Reducer } from 'redux';
import { AppAction } from '../AppAction';

const max_num_length = 18;

type numberState = {
  expression: string;
};

const initialState: numberState = {
  expression: "",
};

export const numberReducer: Reducer<numberState, AppAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case 'ENTER_NUMBER':
      if(action.value === ""){
        state.expression = "";
        return state;
      }
      if((state.expression.includes(".") && action.value === ".") || state.expression.length === max_num_length){
        return state;
      }
      state.expression += action.value
      return state;
    default:
      return state;
  }
};
