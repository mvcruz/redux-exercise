type EnterNumberAction = {
    type: 'ENTER_NUMBER';
    value: string;
};

export const numberOperation = (k: string): EnterNumberAction => ({
    type: 'ENTER_NUMBER',
    value: k,
});

