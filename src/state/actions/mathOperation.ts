type EnterMathOperationAction = {
    type: 'SUM' | 'SUB' | 'MUL' | 'DIV' | 'SUM_ALL' | 'SQRT';
    stack: number[];
};

export const enterSum = (s: number[]): EnterMathOperationAction => ({
    type: 'SUM',
    stack: s,
});

export const enterSub = (s: number[]): EnterMathOperationAction => ({
    type: 'SUB',
    stack: s,
});

export const enterMul = (s: number[]): EnterMathOperationAction => ({
    type: 'MUL',
    stack: s,
});

export const enterDiv = (s: number[]): EnterMathOperationAction => ({
    type: 'DIV',
    stack: s,
});

export const enterSqrt = (s: number[]): EnterMathOperationAction => ({
    type: 'SQRT',
    stack: s,
});

export const enterSumAll = (s: number[]): EnterMathOperationAction => ({
    type: 'SUM_ALL',
    stack: s,
});